;;; -*- Mode: Lisp; Syntax: Common-Lisp -*-
(in-package :crawler)

(defun make-date (year month day)
  (format nil "~4,'0d-~2,'0d-~2,'0d" year month day))

(defun parse-date (str-date)
  (when (stringp str-date)
    (mapcar #'parse-integer (ppcre:split "-" str-date))))

(defun leap-year-p (year)
  (and (= (mod year 4) 0)
       (or (/= (mod year 100) 0)
           (= (mod year 400) 0))))

(defun days-in-month (month year)
  (case month
    (2 (if (leap-year-p year) 29 28))
    ((4 6 9 11) 30)
    (t 31)))

(defun dates-difference (&key (start) (end))
  "A function to count number of days between two dates
   Input have to be a date string: 2016-01-01
  "
  (let ((y1 (car (parse-date start)))
        (y2 (car (parse-date end)))
        (m1 (second (parse-date start)))
        (m2 (second (parse-date end)))
        (d1 (car (last (parse-date start))))
        (d2 (car (last (parse-date end)))))
    (let ((diff (- (encode-universal-time 0 0 12 d2 m2 y2)
                   (encode-universal-time 0 0 12 d1 m1 y1))))
      (/ (/ (/ diff 60) 60) 24))))
