;;; -*- Mode: Lisp; Syntax: Common-Lisp -*-
;;;; crawler.lisp

(in-package #:crawler)

(defun parse-data (data)
  (ignore-errors (parse-integer data)))

(defun search-by-date (id start-date end-date)
  "A Function to fetch number of invoices by date"
  (setq drakma:*text-content-types*
      (cons '("application" . "json") drakma:*text-content-types*))
  (setf drakma:*drakma-default-external-format* :utf-8)
  (parse-data
   (drakma:http-request
    (format nil "http://~A/~A" *server-host* "facturas")
    :method :get
    :parameters `(("id" . ,id)
                  ("start" . ,start-date)
                  ("finish" . ,end-date)))))

;; Search by month and Year
(defun search-by-list-months (id months-years)
  "Function to retrieve number of invoices giving a list of months
  "
  (let ((res '())
        (num 0))
    (loop :for lst in months-years
       :do (loop :with req := 0
              :with max := (days-in-month (first lst) (car (last lst)))
              :for n :downfrom max :to 1
              :for total := (let ((year (car (last lst)))
                                  (day (first lst)))
                              (search-by-date id (make-date year
                                                            day
                                                            1)
                                              (make-date year
                                                         day
                                                         n)))
              :until (numberp total)
              :do (incf req)
              :finally (let ((year (car (last lst)))
                             (day (first lst)))
                         (if (< n max)
                             (progn (incf total
                                          (search-by-date id
                                                          (make-date year
                                                                     day
                                                                     (1+ n))
                                                          (make-date year
                                                                     day
                                                                     max)))
                                    (incf req))))
                (push total res)
                (incf num req)))
    (values res num)))

(defun search-by-year (id &key (year 2017))
  "A method to fetch client invoices during a year
   params: id
   params: year
  "
  (loop
     :with invoices := '()
     :with bad-months := '()
     :with num := 0
     :for month :from 1 :to 12
     :with last-day-month := (days-in-month month year)
     :for total := (search-by-date id
                                    (make-date year month 1)
                                    (make-date (if (= 13 month) (1+ year) year)
                                               (if (= 13 month) 1 month)
                                               last-day-month))
     :do (if (numberp total)
             (push total invoices)
             (push '(month year) bad-months))
         (incf num)
     :finally (return (values invoices (nreverse bad-months) num))))


(defun search-by-range (id &key (start) (end))
  "A function to select a search range
   @params id
   @params start
   @params end
  "
  (let* ((dat1 (parse-date start))
        (dat2 (parse-date end))
        (year1 (first dat1))
        (year2 (first dat2))
        (month1 (second dat1))
        (month2 (second dat2))
        (day1 (car (last dat1)))
        (day2 (car (last dat2)))
        (res '()))
    (loop
      :for day :from (apply #'encode-universal-time
                            (append '(0 0 0) (nreverse dat1)))
        :to (apply #'encode-universal-time
                   (append '(0 0 0 31) (cdr (nreverse dat2))))
       :by (* 32 60 60 24)
       :do (let ((initdate (if (null res) day1 1)))
             (multiple-value-bind (se mi ho da mo ye)
                 (decode-universal-time day)
               (push (format nil "~4,'0d-~2,'0d-~2,'0d" ye mo initdate) res))))
    (multiple-value-bind (result reqs mo)
        (loop :with num := 0
           :with invoices := '()
           :with bad-months := '()
           :for mindate in (nreverse res)
           :for total := (let ((m (parse-integer (subseq mindate 5 7)))
                               (y (parse-integer mindate
                                                 :end (search "-" mindate))))
                           (search-by-date id mindate
                                           (make-date y m
                                                      (if (and (= m month2)
                                                               (= y year2))
                                                          day2
                                                          (days-in-month m y)))))
           :do (if (numberp total)
                   (push total invoices)
                   (push (cdr (nreverse (parse-date mindate))) bad-months))
             (incf num)
           :finally
             (return (values (nreverse invoices) num (nreverse bad-months))))
      (if (not (null mo))
          (multiple-value-bind (a b)
              (search-by-list-months id mo)
            (nconc result a)
            (incf reqs b)))
      (format nil "{\"result\":\"~A\", \"requests\":\"~A\"}"
              (reduce #'+ result) reqs))))

(defun find_all_bills_by_year (id &key (year 2017))
  (multiple-value-bind (total bad-months requests)
      (search-by-year id :year year)
    (if (not (null bad-months))
        (multiple-value-bind (a b)
            (search-by-list-months id bad-months)
          (push total a)
          (incf requests b)))
    (cond ((null total)
           (format nil "{\"result\":\"~A\", \"requests\":\"~A\"}" 0 requests))
          (t (format nil "{\"result\":\"~A\", \"requests\":\"~A\"}"
                     (apply #'+ total) requests)))))


(defun best-range (&key (start) (end) (div 20))
  (let ((num-days (dates-difference :start start :end end)))
    (values (round (float (/ num-days div))) div)))

(defun specific-range (id &key (start) (end) (div))
  "Function to read from a specific range and create a rang of a num of days"
  (let* ((total '())
         (reqs 0)
         (dat1 (parse-date start))
         (dat2 (parse-date end))
         (time1 (apply #'encode-universal-time (append'(0 0 0) (nreverse dat1))))
         (time2 (apply #'encode-universal-time (append'(0 0 0) (nreverse dat2)))))
    (loop
      :for i :from time1 :to time2 :by (* div 60 60 24)
      :do
         (let* ((next (+ i (- (* div 60 60 24) (* 60 60 24))))
                (day1 (multiple-value-bind (se mi ho da mo ye)
                          (decode-universal-time i)
                        (format nil "~4,'0d-~2,'0d-~2,'0d" ye mo da)))
                (day2 (multiple-value-bind (se mi ho da mo ye)
                          (decode-universal-time next)
                        (setq day2 (format nil "~4,'0d-~2,'0d-~2,'0d" ye mo da)))))
           (if (> next time2)
               (setq day2 end))
           (push (search-by-date id day1 day2) total)
           (incf reqs)))
    (format nil "{\"result\":\"~A\", \"requests\":\"~A\"}"
            (apply #'+ total) reqs)))

;; Main function
(defun main ()
  (cond
    ((= (length sb-ext:*posix-argv*) 2)
     (print (find_all_bills_by_year
             (nth 1 sb-ext:*posix-argv*))))
    ((and (= (length sb-ext:*posix-argv*) 3)
          (not (nil
                (parse-integer (nth 2 sb-ext:*posix-argv*) :junk-allowed t))))
     (print (find_all_bills_by_year
             (nth 1 sb-ext:*posix-argv*)
             :year (parse-integer (nth 2 sb-ext:*posix-argv*)))))
    ((= (length sb-ext:*posix-argv*) 4)
     (print (specific-range (nth 1 sb-ext:*posix-argv*)
                            :start (nth 2 sb-ext:*posix-argv*)
                            :end (nth 3 sb-ext:*posix-argv*)
                            :div 27)))
    (t
     (print
      (format nil "{\"result\":\"~A\", \"requests\": \"~A\"}~%"
              "Hello i am crawler! There is nothing to search" 0)))))

(format t "~%;;; Done")
