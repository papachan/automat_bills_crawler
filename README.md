# Example Crawler in Lisp

## Concepts:

An example crawling a specific service through a web url using Common lisp.

Why Common Lisp? Because its a wonderful multiparadigm programming
language. its fast and powerful. Very practical working with
algorithms and data structures.

![made with lisp](http://weitz.de/regex-coach/lisp_logo.jpg)

## Usage:

How to run the project:

If your local directory is cloned into quicklisp system projects, you can directly load this project using quicklisp.

```
(ql:quickload :crawler)

```

## Requirements:

How to install sbcl compiler for ANSI Common Lisp:

With linux platform you can search sbcl unsing apt-search

Or just install with:

```
$ apt-get install sbcl
```

With mac you can use homebrew:

```
$ brew install sbcl
```


How to install quicklisp:

* Download quicklisp and quicklisp.lisp.asc
* Install quicklisp in your ~/.local directory
* Install slime-helper.el for Slime plugin for Emacs

```
$ curl -O http://beta.quicklisp.org/quicklisp.lisp
$ curl -O http://beta.quicklisp.org/quicklisp.lisp.asc
$ gpg --verify quicklisp.lisp.asc
$ sbcl --load quicklisp.lisp
$ mkdir -p ~/.local/opt
```

Inside the SBCL repl:
```
> (load "~/tmp/quicklisp.lisp")
> (quicklisp-quickstart:install :path "~/.local/opt/quicklisp")
> (ql:add-to-init-file)
> (ql:quickload "quicklisp-slime-helper")
> (quit)
```

## Run all tests

```
(ql:quickload :crawler-test)
```

OR:

```
(asdf:test-system 'crawler-test)
```

## To run a docker image:

To start SBCL repl at your local and run the code just type:

```
$ docker run --rm -it -v ~/dev/source_code/quicklisp/crawler/:/usr/local/share/common-lisp/source daewok/lisp-devel:latest sbcl
```

_(in my case my local folder is: ~/dev/source_code/quicklisp/)_

## Build an executable:

At root directory you have a makefile to create an binary executable.

```
$ make
```

Then you can call the executable from command line:


```
$ ./crawler
```

Run it:

```
$ ./crawler 4e25ce61-e6e2-457a-89f7-116404990967 2017-01-01 2017-01-05
```

## Author

If you have any suggestions feel free to contribute to this project or
just contact me for any issues. I will be happy to improve my code or
offer new features.

+ Dan Loaiza <papachan@gmail.com>
+ tappsi.co - easytaxi.com

### git tool:

* magit (emacs)

## Copyright

Copyright (c) 2018 Dan Loaiza <papachan@gmail.com>

## License

Licensed under the BSD License.
