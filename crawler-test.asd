(defsystem crawler-test
  :author "@papachan <papachan@gmail.com>"
  :version "0.1"
  :license "BSD"
  :depends-on (:fiveam
               :drakma
               :cl-json
               :crawler)
  :components ((:module "t"
                :serial t
                :components
                ((:file "package")
                 (:file "tests"))))
  :perform (test-op (o s)
                    (uiop:symbol-call :fiveam '#:run!
                                      (uiop:find-symbol* '#:all-tests crawler-test)))
  :description "")
