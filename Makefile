# A makefile for creating a standalone executable Common Lisp image.
#

buildapp:
	sbcl --load crawler.asd \
                --eval '(ql:quickload :crawler)' \
                --eval "(sb-ext:save-lisp-and-die #p\"crawler\" :toplevel #'crawler:main :executable t)"
