;;; -*- Mode: Lisp; Syntax: Common-Lisp -*-
;;;; tests.lisp
(asdf:oos 'asdf:load-op :fiveam)

(defpackage crawler-test
  (:use :cl
        :fiveam
        :drakma
        :cl-json
        :crawler)
  (:import-from :crawler
                :*server-host*
                :+id+
                :search-by-list-months
                :search-by-year
                :search-by-date
                :search-by-range
                :dates-difference
                :best-range
                :specific-range)
  (:import-from :cl-json
                :decode-json))

(in-package :crawler-test)

(def-suite all-tests
  :description
  "some tests to validate each functions at our crawler")

(in-suite all-tests)

(test day_difference_test
  "does lisp count the correct number of days?"
  (is (= 7 (dates-difference :start "2017-02-21" :end "2017-02-28"))))

(test check_best_range
  "Calculate the best range to parse two range date"
  (is (= 3 (best-range :start "2016-01-01" :end "2016-02-24"))
      (= 2 (best-range :start "2016-01-01" :end "2016-02-24" :div 27))))

(test crawl-specific-range
  "test a specific date to crawl"
  (let ((start-date "2017-01-01")
        (end-date "2017-06-22"))
    (with-input-from-string
        (s (specific-range +id+ :start start-date :end end-date :div 27))
      (let ((data (decode-json s)))
        (is (= 525 (parse-integer (rest (assoc :result data)))))
        (is (= 7 (parse-integer (rest (assoc :requests data)))))))))

(test is_server_alive
  "check if server is alive"
  (is (= 200 (multiple-value-bind (body status)
             (drakma:http-request (format nil "http://~a" *server-host*))
               status))))

(test one_date_invoices
  "crawl web service to return one year invoices"
  (is (= 47 (search-by-date +id+ "2017-01-01" "2017-01-12"))))

(test many_months_invoices
  "crawl web service with a list of months"
  (multiple-value-bind (total requests)
      (search-by-list-months +id+ '((1 2017) (3 2017) (7 2017) (8 2017)))
    (is (= 417 (reduce #'+ total)))
    (is (= 7 requests))))

(test one_year_invoices
  "crawl server to return one year invoices"
  (multiple-value-bind (total m)
      (search-by-year +id+ :year 2017)
    (is (= 483 (reduce #'+ total)))))

(test specific_range_invoices
  "crawl web service when it search multiple years"
  (let ((start-date "2016-12-01")
        (end-date "2017-05-15"))
    (with-input-from-string
        (s (search-by-range +id+ :start start-date :end end-date))
      (let ((data (decode-json s)))
        (is (= 420 (parse-integer (rest (assoc :result data)))))
        (is (> (parse-integer (rest (assoc :requests data))) 0)))))
  (let ((start-date "2017-01-01")
        (end-date "2018-01-12"))
    (with-input-from-string
        (s (search-by-range +id+ :start start-date :end end-date))
      (let ((data (decode-json s)))
        (is (= 1173 (parse-integer (rest (assoc :result data)))))
        (is (> (parse-integer (rest (assoc :result data))) 0))))))

(test specific_month_range_invoices
  "search by range should fix more than 100 problem"
  (let ((start-date "2017-04-01")
        (end-date "2017-09-01"))
    (with-input-from-string
        (s (search-by-range +id+ :start start-date :end end-date))
      (let ((data (decode-json s)))
        (is (= 482 (parse-integer (rest (assoc :result data)))))
        (is (> (parse-integer (rest (assoc :requests data))) 10))))))

(test is_json_response
  "our service answer with a valid json"
  (let ((start-date "2017-01-01")
        (end-date "2017-01-15"))
    (with-input-from-string
        (s (search-by-range +id+ :start start-date :end end-date))
      (is (not (null (decode-json s)))))))

(run! 'all-tests)

(print "tests ends")
