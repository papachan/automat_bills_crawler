;;; -*- Mode: Lisp; Syntax: Common-Lisp -*-
(defpackage crawler-test
  (:use :cl
        :fiveam
        :drakma
        :cl-json
        :crawler)
  (:export :run!
           :all-tests))
