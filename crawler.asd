#|
   author Dan Loaiza <papachan@gmail.com>
|#

(in-package :cl-user)
(defpackage :crawler-asd
  (:use :cl :asdf))
(in-package :crawler-asd)

(asdf:defsystem :crawler
  :version "1.0"
  :author "Dan Loaiza <papachan@gmail.com>"
  :license "BSD"
  :serial t
  :depends-on (:drakma
               :cl-ppcre)
  :components ((:file "package")
               (:file "date")
               (:file "config")
               (:file "crawler"))
  :description "")
